// =================================================================================================
//
//	CopyEngine Framework
//	Copyright 2012 Eran. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================

package copyengine.utils.packerbox.data
{
	import flash.display.BitmapData;
	import flash.geom.Rectangle;

	public final class CEPackerElementData
	{
		public var bitmapData:BitmapData;
		public var bitmapDataName:String;

		//当前Texture所在的矩形位置
		public var bitmapDataRe:Rectangle;


		public function CEPackerElementData()
		{
		}

		public function clone():CEPackerElementData
		{
			var newData:CEPackerElementData=new CEPackerElementData();
			newData.bitmapData=bitmapData; //bitmapData不会发生改变 不用动
			newData.bitmapDataName=bitmapDataName;
			newData.bitmapDataRe=bitmapDataRe.clone();
			return newData;
		}

	}
}
