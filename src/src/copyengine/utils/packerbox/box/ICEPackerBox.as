// =================================================================================================
//
//	CopyEngine Framework
//	Copyright 2012 Eran. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================


package copyengine.utils.packerbox.box
{
	import copyengine.utils.packerbox.data.CEPackerElementData;

	public interface ICEPackerBox
	{
		function initialize(_boxSize:int, _boxName:String):void;
		function tryToAddToBox(_element:CEPackerElementData):Boolean;
		function tryToNarrowDownBox():void;

		function getBoxSize():int;
		function getBoxName():String;
		function getAllBoxElement():Vector.<CEPackerElementData>;
	}
}
